module.exports.run = async (client, message, args) => {
    if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel');
    if (!message.member.voiceChannel.permissionsFor(message.guild.me).has('CONNECT')) return message.channel.send('I dont have the permission to join your voice channel..');
    let volume = 1;
    const parsed = parseFloat(args[0]);
    if (parsed && parsed > 0 && parsed < 20) volume = parsed;
    const voiceChannel = message.member.voiceChannel;
    voiceChannel.join().then(connection => {
        const dispatcher = connection.playFile('./music/airhorn.mp3');
        dispatcher.setVolume(volume);
        dispatcher.on('end', () => {
            voiceChannel.leave();
        });
    });

};


exports.help = {
    name: 'airhorn',
    category: 'entertainment',
    example: 'airhorn',
    description: 'plays a airhorn sound in your current voice channel',
};