const $console = require('Console');
module.exports = async (client) => {
    client.ready = true;
    if (client.settings.has('lastMessage')) {
        const dat = client.settings.get('lastMessage');
        const msg = await client.channels.get(dat.channel).fetchMessage(dat.msg);
        msg.edit(`\`\`\`css\n${dat.content}\`\`\`*restart completed*`);
        client.settings.delete('lastMessage');
    }
    let counter = 0;
    $console.success(`client is ready after ${process.uptime() * 1000 - client.uptime} Milliseconds`);
    $console.success(`${client.channels.size} channels on ${client.guilds.size} servers, for a total of ${client.guilds.map(g => g.memberCount).reduce((a, b) => a + b)} users.`);
    $console.log(`logged in as ${client.user.tag}`);
    setInterval(changing_status, 12001);
    const status = [`${client.guilds.size} Guilds`, 'Tag me for Info', `${client.guilds.map(g => g.memberCount).reduce((a, b) => a + b)} Users`, `${client.channels.size} Channels`, '$invite to get a invite Link'];

    function changing_status() {
        counter++;
        if (counter === status.length) counter = 0;
        client.user.setActivity(status[counter]);
    }
};