const Discord = require('discord.js');
const prefix = '$';
module.exports = async (client, message) => {
    if (message.channel.type == 'dm') return;
    if (message.author.bot) return;
    const prefixRegex = new RegExp(`^(<@!?${client.user.id}>)`);
    if (prefixRegex.test(message.content)) {
        message.channel.send(new Discord.RichEmbed()
            .setColor(client.config.cn)
            .addField('Info', `Prefix on this Server: \`${prefix}\`\nDo \`${prefix}help\` for more information`));
    }
    if (message.content.indexOf(prefix) !== 0) return;
    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    const cmd = client.commands.get(command);
    if (cmd) cmd.run(client, message, args);

};